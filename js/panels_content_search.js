(function($) {

  /**
   * Panels content panes JS search.
   */
  Drupal.behaviors.panelsContentSearch = {
    // Search expression is stored globally since CTools completely replaces
    // modal content on category change.
    searchKey: '',
    attach: function (context, settings) {
      $(context)
        .find('.panels-content-search')
        .each(function() {
          var $search = $(this),
            index = $search.data('search-index'),
            currentIndex = Drupal.behaviors.panelsContentSearch.updateIndex(index, Drupal.behaviors.panelsContentSearch.searchKey),
            $modal = $search.closest('.panels-add-content-modal'),
            $input = $search.find('.panels-content-search-input'),
            $categories = $modal.find('.panels-section-column-categories'),
            $columns = $modal.find('.panels-section-columns');

          var runSearch = function () {
            var $this = $(this);
            Drupal.behaviors.panelsContentSearch.searchKey = $this.val().toLowerCase();
            currentIndex = Drupal.behaviors.panelsContentSearch.updateIndex(index, Drupal.behaviors.panelsContentSearch.searchKey);
            Drupal.behaviors.panelsContentSearch.updateCategories($categories, currentIndex);
            Drupal.behaviors.panelsContentSearch.updateColumns($columns, Drupal.behaviors.panelsContentSearch.searchKey);
          };

          $(this).once('panelsContentSearch', function () {
            Drupal.behaviors.panelsContentSearch.initCategories($categories);
            Drupal.behaviors.panelsContentSearch.updateCategories($categories, currentIndex);

            $input.bind('keyup', runSearch);
            $(document).one("CToolsDetachBehaviors", function() {
              // Reset search expression after modal is closed.
              Drupal.behaviors.panelsContentSearch.searchKey = '';
            });
          });

          // Restore input state after category is changed.
          $input.val(Drupal.behaviors.panelsContentSearch.searchKey);
          Drupal.behaviors.panelsContentSearch.updateColumns($columns, Drupal.behaviors.panelsContentSearch.searchKey);
        })
    },
    /**
     * Build new search index, excluding items not matching current search.
     */
    updateIndex: function (index, search) {
      var newIndex = {};
      $.each(index, function (category, panes) {
        newIndex[category] = {};
        $.each(panes, function(pane, searched) {
          if (searched.indexOf(search) !== -1) {
            newIndex[category][pane] = searched;
          }
        });
      });
      return newIndex;
    },
    /**
     * Init categories list before any search is performed.
     */
    initCategories: function ($categories) {
      $categories
        .find('.panels-modal-add-category')
        .each(function () {
          var $row = $(this);
          // Store category title as a data attribute before the counter div
          // is appended.
          $row.data('title', $(this).text().toLowerCase());
        })
        .append('<div class="panels-search-counter"></div>');
    },
    /**
     * Update categories list.
     */
    updateCategories: function ($categories, searchIndex) {
      // Reset counters if key is empty.
      $categories
        .find('.panels-modal-add-category')
        .each(function() {
          var count = 0,
            title = $(this).data('title');
          if (searchIndex[title]) {
            count = Drupal.behaviors.panelsContentSearch.countObjectProperties(searchIndex[title]);
          }
          Drupal.behaviors.panelsContentSearch.setCategoryCounter($(this), count);
        });
    },
    /**
     * Update counter in the category selection element.
     */
    setCategoryCounter: function ($category, count) {
      if (count) {
        $category.removeClass('panels-category-zero-results');
      }
      else {
        $category.addClass('panels-category-zero-results');
      }
      $category
        .find('.panels-search-counter')
        .text('(' + count + ')');
    },
    /**
     * Update content panes list hiding those do not match search.
     */
    updateColumns: function ($columns, searchKey) {
      var found = false;
      $columns.find('.content-type-button').each(function(i, elem) {
        if ($(elem).text().toLowerCase().search(searchKey) > -1) {
          $(elem).show();
          found = true;
        }
        else {
          $(elem).hide();
        }
      });

    },
    /**
     * Helper function for counting object properties.
     */
    countObjectProperties: function (object) {
      var count = 0;
      for (var i in object) {
        if (object.hasOwnProperty(i)) {
          count++;
        }
      }
      return count;
    }
  };

})(jQuery);
